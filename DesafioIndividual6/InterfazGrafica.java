package DesafioIndividual6;

import java.awt.EventQueue;

import javax.swing.JFrame;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JButton;

import java.awt.event.ActionListener;

import java.awt.event.MouseEvent;

import java.awt.event.MouseMotionListener;

import java.awt.event.ActionEvent;

public class InterfazGrafica {

//	PROGRAMA BASICO DE RODRIGO NAHUEL KIRSCH CREADO CON INTERFAZ DE ECLIPSE 
//	EL PROGRAMA RECONOCE SI SE MUEVE EL MOUSE O NO DENTRO DEL FRAME Y EMITE UN MENSAJE POR CONSOLA DEPENDIENDO DE LA ACCION
//	RECONOCE QUE BOTON SE HA PRESIONADO Y EMITE UN MENSAJE POR CONSOLA (SIMILAR A HA GANADO EL FOCO O LO HA PERDIDO)
	
	
	private JFrame frame;
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {

			public void run() {
			
				try {
				
					InterfazGrafica window = new InterfazGrafica();
					
					window.frame.setVisible(true);
				
				} catch (Exception e) {
				
					e.printStackTrace();
				
				}
			
			}
		
		});
		
	}

	public InterfazGrafica() {
	
		initialize();
	
	}

	private void initialize() {
	
		frame = new JFrame();
		
		frame.setBounds(100, 100, 450, 300);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setVisible(true);
		
		JPanel panel = new JPanel();
		
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		Raton eventoraton=new Raton();
		
		panel.addMouseMotionListener(eventoraton);
	
		
		JButton btnNewButton_1 = new JButton("Noob Boton");
		
		btnNewButton_1.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent e) {
			
				System.out.println("Usted es un Noob");
				
			}
		
		});
		
		panel.add(btnNewButton_1);
		
		JButton btnNewButton = new JButton("Pro Boton");
		
		btnNewButton.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent e) {
			
				System.out.println("Usted es un Pro");
			
			}
		
		});
		
		panel.add(btnNewButton);
		
			
		}
	
}

class Raton implements MouseMotionListener{

	@Override

	public void mouseDragged(MouseEvent arg0) {
		
		// TODO Auto-generated method stub
	
		System.out.println("Esta arrastrando el mouse");
	
	}

	@Override
	
	public void mouseMoved(MouseEvent arg0) {
	
		// TODO Auto-generated method stub
		
		System.out.println("Se ha movido el mouse");
	
	}

}