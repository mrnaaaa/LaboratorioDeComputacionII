package TrabajoIndividual;

import java.io.Serializable;

@SuppressWarnings("serial")

public class CuentaCorriente  extends Banco implements  Serializable{
	
	private String titular;
	
	private double saldo;
	
	private int numacc;
	
	public String getTitular() {
		
		return titular;
		
	}
	
	public CuentaCorriente(String titular, double saldo, int numacc) {

		this.titular = titular;
		
		this.saldo = saldo;
		
		this.numacc = numacc;
	
	}

	public void setTitular(String titular) {
		
		this.titular = titular;
		
	}
	
 	public double getSaldo() {                      //Metodo que devuelve el valor de la cuenta
		
		return saldo;
		
	}
	
	public void setSaldo(double dinero) {            //Metodo para ingresar dinero
		
		this.saldo = saldo+dinero;
		
	}
	
     public double extraccion(double dinero) {           //Metodo que realiza la extraccion de una cuenta
    	 
    	 this.saldo= saldo-dinero;
    	 
    	 return saldo;
     }
	
	public int getNumacc() {
		
		return numacc;
	}
	
	public void setNumacc(int numacc) {
		
		this.numacc = numacc;
		
	}

	@Override
	
	public String toString() {
		
		return "CuentaCorriente [titular=" + titular + ", saldo=" + saldo + ", numacc=" + numacc + "]";
	}
	
}