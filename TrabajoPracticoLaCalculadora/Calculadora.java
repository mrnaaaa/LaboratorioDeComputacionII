package TrabajoPracticoLaCalculadora;    

import javax.swing.*;
	
import java.awt.*;
	
import java.awt.event.ActionEvent;
	
import java.awt.event.ActionListener;
	
	public class Calculadora{
		
		public static void main(String[] args) {
			
			marco calculator = new marco();
	        
			calculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		}
	
	}
	
	class marco extends JFrame{
		
		public marco() {
	        
			setTitle("Calculadora");
	        
	        setBounds(400, 300, 400, 300);
	        
	        add(new panel());
	        
	        setVisible(true);
	
		}
		
	}
	
	class panel extends JPanel{
		
		private JButton pantalla;
		
		private JPanel panelnumerico;
		
		private boolean inicio;
		
		private double resultado;
		
		private String ultimaOperacion;
		
		String cerebro;
		
		public panel () {
		
			inicio=true;
			
			setLayout(new BorderLayout());

	        pantalla = new JButton("0");
	        
	        pantalla.setEnabled(false);
	        
	        add(pantalla, BorderLayout.NORTH);
	        
	        panelnumerico=new JPanel();
	        
	        panelnumerico.setLayout(new GridLayout(4, 4));
		
	        ActionListener insertanumero=new numero();
	        
	        ActionListener operacion= new operaciones();
	        
	        ActionListener borradorDeDisplay=new borrarDisplay();
	        
	        nuevoBoton("7",insertanumero );
	        
	        nuevoBoton("8", insertanumero);
	        
	        nuevoBoton("9", insertanumero);
	        
	        nuevoBoton("*", operacion);
	        
	        nuevoBoton("4",insertanumero);
	        
	        nuevoBoton("5", insertanumero);
	        
	        nuevoBoton("6", insertanumero);
	        
	        nuevoBoton("-", operacion);
	        
	        nuevoBoton("1", insertanumero);
	        
	        nuevoBoton("2", insertanumero);
	        
	        nuevoBoton("3", insertanumero);
	        
	        nuevoBoton("+", operacion);
	        
	        nuevoBoton("=", operacion);
	        
	        nuevoBoton("0", insertanumero);
	        
	        nuevoBoton("C",borradorDeDisplay );
            
	        nuevoBoton("/", operacion);
 
			add(panelnumerico, BorderLayout.CENTER);
			
			ultimaOperacion="=";
			
		}

		private void nuevoBoton(String caracter, ActionListener oyente) {
			
			JButton boton=new JButton(caracter);
			
			boton.addActionListener(oyente);
			
			panelnumerico.add(boton);
		}
		
		private class numero implements ActionListener{

			@Override
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				String entrada=e.getActionCommand();
				
				if(inicio) {
					
					pantalla.setText("");
					
					inicio=false;
				}
				
				pantalla.setText(pantalla.getText()+entrada);
				
			}
			
		}
		
		 class operaciones implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				        cerebro=e.getActionCommand();
				 
				
						calculator(Double.parseDouble(pantalla.getText()));
					
						ultimaOperacion=cerebro;
						 
						inicio=true;
					
				
			}
			
			
			public void calculator(double nn) {
				
				if(ultimaOperacion.equals("+")) {
					
					resultado +=nn;
					
				}
				
				else if(ultimaOperacion.equals("-")) {
					
					resultado -=nn;
					
				}
                
				else if(ultimaOperacion.equals("*")) {
					
					resultado *=nn;
					
				} else if(ultimaOperacion.equals("C")) {
					
					resultado =0;
					
				}
                
				else if(ultimaOperacion.equals("/")) {
					
                	 if(nn==0) {
					
                		 resultado=-1;
						
                		 pantalla.setText("No se puede dividir por 0");
						
					}
					
                	 else {
					
                		 resultado /=nn;
					
					}
				
                 }
                 
				else if (ultimaOperacion.equals("=")) {
					
					resultado=nn;
					
				}
				
				if(resultado<0) {
					
				
				
					pantalla.setText("No se puede dividir por 0");
				
				}
				
				else {
				
					pantalla.setText(""+resultado);
				
				}
			
			}
		
		}
	   
		 private class borrarDisplay implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			pantalla.setText(" ");
		
		}
		   
	   }
	}