package TtrabajoIndividialProcesadorDeTexto;

import java.awt.BorderLayout;

import java.awt.Dimension;

import java.awt.Font;

import java.awt.Toolkit;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import java.awt.event.InputEvent;

import java.awt.event.KeyEvent;

import java.awt.event.WindowAdapter;

import java.awt.event.WindowEvent;

import java.io.File;

import java.io.FileNotFoundException;

import java.io.FileOutputStream;

import java.io.FileReader;

import java.io.IOException;

import java.io.ObjectOutputStream;

import javax.swing.ButtonGroup;

import javax.swing.JFileChooser;

import javax.swing.JFrame;

import javax.swing.JMenu;

import javax.swing.JMenuBar;

import javax.swing.JMenuItem;

import javax.swing.JOptionPane;

import javax.swing.JPanel;

import javax.swing.JPopupMenu;

import javax.swing.JRadioButtonMenuItem;

import javax.swing.JScrollPane;

import javax.swing.JTextPane;

import javax.swing.KeyStroke;

import javax.swing.text.StyledEditorKit;

import java.awt.Image;

import java.lang.Exception;

public class ModeloDeProcesadorDeTexto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		marco1 marconuevo1= new marco1();
		
        marconuevo1.setVisible(true);
        
		marconuevo1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class marco1 extends JFrame{

	public marco1(){
		
		setTitle("Procesador de Texto - Kirsch Rodrigo Nahuel");
		
		//setBounds(500,500,450,450);
         
		Toolkit mipantalla=Toolkit.getDefaultToolkit();
		
		Dimension tamaņopantalla=mipantalla.getScreenSize();
		
		int alturapantalla=tamaņopantalla.height;
		
		int anchopantalla=tamaņopantalla.width;
		
		setSize(anchopantalla/2,alturapantalla/2);
		
		setLocation(anchopantalla/4,alturapantalla/4);
		
        panel1 mipanel1=new panel1();
		
		add(mipanel1);
		
		try {
			
			 Image miicono1=mipantalla.getImage("C:\\Users\\RINZLER\\Desktop\\arqysistop/DWG-FAKER.jpg");
			   
			   setIconImage(miicono1);
			   
			}catch(Exception e){
				
				e.printStackTrace();
				
			}
		
		 addWindowListener(new WindowAdapter() {
	        	
	        	@Override
	        	public void windowClosing(WindowEvent e) {
	        		
	        		JOptionPane.showMessageDialog(null, " Se guardara el proceso y despues se cerrara el programa");
	        		
	        		panel1.guardarArchivos();
	        			
	        		System.exit(0);
	        		
	        	}
	        	
	        });
	
	}
		
}

class panel1  extends JPanel {

	static JTextPane texto=new JTextPane();
	
	JMenu archivo,fuente,estilo,tamanio;
	
	Font letras;
	
	public panel1() {
		
		setLayout(new BorderLayout());
		
		JPanel menu=new JPanel();
		
		JMenuBar barra=new JMenuBar();
		
		archivo=new JMenu("Archivo");
		
	    fuente=new JMenu("Fuente");
		
	    estilo=new JMenu("Estilo");
		
	    tamanio=new JMenu("Tamaņo");
	    
	    JMenuItem abrir=new JMenuItem("Abrir");
	    
	    JMenuItem guardar=new JMenuItem("Guardar");
		
	    configuracionDeMenu("Times New Roman", "fuente", "Times New Roman", 1, 1);
	    
	    configuracionDeMenu("Arial", "fuente", "Arial", 1, 1);
	    
	    configuracionDeMenu("Verdana", "fuente", "Verdana", 1, 1);
	    
	    configuracionDeMenu("Calibri", "fuente", "Calibri", 1, 1);
	    
	    configuracionDeMenu("Negrita", "estilo", " ", Font.BOLD, 1);
	    
	    configuracionDeMenu("Cursiva", "estilo", " ", Font.ITALIC, 1);

	    ButtonGroup tamanioLetra= new ButtonGroup();
	    
	    JRadioButtonMenuItem t12 = new JRadioButtonMenuItem("12");
	    
	    JRadioButtonMenuItem t16 = new JRadioButtonMenuItem("16");
	    
	    JRadioButtonMenuItem t20 = new JRadioButtonMenuItem("20");
	    
	    JRadioButtonMenuItem t24 = new JRadioButtonMenuItem("24");
		
	    JRadioButtonMenuItem t28 = new JRadioButtonMenuItem("28");
	    
	    JRadioButtonMenuItem t32 = new JRadioButtonMenuItem("32");
	    
	    tamanioLetra.add(t12);
	    
	    tamanioLetra.add(t16);
	    
	    tamanioLetra.add(t20);
	    
	    tamanioLetra.add(t24);
	    
	    tamanioLetra.add(t28);
	    
	    tamanioLetra.add(t32);
	    
	    t12.addActionListener(new StyledEditorKit.FontSizeAction("CambioTamanio", 12));
	    
	    t16.addActionListener(new StyledEditorKit.FontSizeAction("CambioTamanio", 16));
	    
	    t20.addActionListener(new StyledEditorKit.FontSizeAction("CambioTamanio", 20));
	    
	    t24.addActionListener(new StyledEditorKit.FontSizeAction("CambioTamanio", 24));
	    
	    t28.addActionListener(new StyledEditorKit.FontSizeAction("CambioTamanio", 28));
	    
	    t32.addActionListener(new StyledEditorKit.FontSizeAction("CambioTamanio", 32));
	    
        tamanio.add(t12);
        
        tamanio.add(t16);
        
        tamanio.add(t20);
        
        tamanio.add(t24);
        
        tamanio.add(t28);
        
        tamanio.add(t32);
        
	    abrir.addActionListener(new abriendoArchivos());
        
	    archivo.add(abrir);
	    
	    guardar.addActionListener(new guardandoArchivo());
	    
	    archivo.add(guardar);
	    
	    barra.add(archivo);
		
		barra.add(fuente);
		
		barra.add(estilo);
		
		barra.add(tamanio);
		
		menu.add(barra);
		
		add(menu,BorderLayout.NORTH);
		
		add(texto,BorderLayout.CENTER);
		
		JScrollPane desplazarTexto= new JScrollPane(texto);
		 
		add(desplazarTexto,BorderLayout.CENTER);
		
		JPopupMenu menuE=new JPopupMenu();

        JMenuItem negritaEmergente=new JMenuItem("Negrita");

        JMenuItem cursivaEmergente=new JMenuItem("Cursiva");

        negritaEmergente.addActionListener(new StyledEditorKit.BoldAction());

        cursivaEmergente.addActionListener(new StyledEditorKit.ItalicAction());

        menuE.add(negritaEmergente);

        menuE.add(cursivaEmergente);

        texto.setComponentPopupMenu(menuE);

	}
	
	public void configuracionDeMenu(String tipotexto,String mimenu,String tipoletra,int tipoestilo,int tam) {
		
		JMenuItem elementoDelMenu= new JMenuItem(tipotexto);
		
		if(mimenu=="fuente") {
			
			fuente.add(elementoDelMenu);
			
			if(tipoletra=="Times New Roman") {
				
				elementoDelMenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiarDeLetra", "Times New Roman"));
				
			}else if(tipoletra=="Arial") {
				
				elementoDelMenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiarDeLetra", "Arial"));
				
			}else if(tipoletra=="Verdana") {
				
				elementoDelMenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiarDeLetra", "Verdana"));
				
			}else if(tipoletra=="Calibri") {
			
			elementoDelMenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambiarDeLetra", "Calibri"));
			
		
			}
			
		}else if(mimenu=="estilo") {
			
			estilo.add(elementoDelMenu);
			
			if(tipoestilo==Font.BOLD) {
				
			elementoDelMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
			
			elementoDelMenu.addActionListener(new StyledEditorKit.BoldAction());
			
			}else if(tipoestilo==Font.ITALIC) {
			
			elementoDelMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));	
				
			elementoDelMenu.addActionListener(new StyledEditorKit.ItalicAction());
			
			}
			
		}else if(mimenu=="tamanio")  {
			
			tamanio.add(elementoDelMenu);
			
			elementoDelMenu.addActionListener(new StyledEditorKit.FontSizeAction("CambioTamanio", tam));
		
	}
			 
	}

	public static void abrirArchivo() {	 		

		JFileChooser seleccionador=new JFileChooser();
		
		int archivoSeleccionado= seleccionador.showOpenDialog(texto);
		
		if(archivoSeleccionado==JFileChooser.APPROVE_OPTION) {
		
			File fichero= seleccionador.getSelectedFile();	
			
			fichero.getAbsolutePath();
			
			try(FileReader lectorArchivos=new FileReader(fichero)){
		        
				String cadena="";
		        
		        int valor=lectorArchivos.read();
		        
		        while(valor!=-1){
		        
		        	cadena=cadena+(char)valor;
		            
		        	valor=lectorArchivos.read();
		        
		        }
			
		        texto.setText(cadena);
		        
		        lectorArchivos.close();
		        
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
			
				e.printStackTrace();
			
				JOptionPane.showMessageDialog(null, "No se ha encontrado el archivo");
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
			
				e.printStackTrace();
				
				JOptionPane.showMessageDialog(null, " I/O ERROR");
			
			}
			
		}
		
	}
	
	 class abriendoArchivos implements ActionListener {
	
		@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
			panel1.abrirArchivo();

}
		
	}
	 
	 public static void guardarArchivos() {
		
		 try {
			 
			ObjectOutputStream saveArch = new ObjectOutputStream(new FileOutputStream(
		
					"C:\\Users\\RINZLER\\prueba\\team-java\\lab2\\src\\TtrabajoIndividialProcesadorDeTexto/documentodeguardado.txt"));
		
			saveArch.writeObject(texto.getText());
			
			saveArch.close();
			
		 } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
		
			e.printStackTrace();
		
			JOptionPane.showMessageDialog(null, "No se ha encontrado el archivo");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
		
			e.printStackTrace();
			
			JOptionPane.showMessageDialog(null, " I/O ERROR");
		
		}
		 
	 }
	
	  class guardandoArchivo implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			panel1.guardarArchivos();
			
		}
		  
	 }
	 
}