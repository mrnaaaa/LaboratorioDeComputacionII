import java.util.Random;
public class desafio11 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CuentaCorriente acc1 = new CuentaCorriente("Fulano", 40000.00);
		CuentaCorriente acc2 = new CuentaCorriente("Mengano", 80000.00);
		CuentaCorriente.Transferencia(acc1, acc2, 2500);	
		/*
		 * Otra forma de mostrar para mi, me quede con el generado porque no sabia eso para usarlo en otros programas
		System.out.println(acc1.imprimir());
		System.out.println(acc2.imprimir());
		*/
		System.out.println(acc1.toString()); 
		System.out.println(acc2.toString());
		}
}
class CuentaCorriente {
	private double balance;
	private String nomacc;
	private long numacc;
   public CuentaCorriente(String nomtit, double balance) {
		this.nomacc = nomtit;
		this.balance = balance;
		Random genacc = new Random();
		this.numacc = Math.abs(genacc.nextLong());
	}
	public CuentaCorriente(String nomtit) {
		this.nomacc = nomtit;
		this.balance = 0;
		Random genacc = new Random();
		this.numacc = Math.abs(genacc.nextLong());
	}
	public String ingresarDiner(double dinero) {
		if(dinero >0) {
			this.balance += dinero;
			return "Se ha ingresado $"+ dinero;
		}else {
			return "no se puede ingresar un valor negativo";
		}
	}
	public void sacarDinero(double dinero) {
		this.balance -= dinero;
	}
	public static void Transferencia(CuentaCorriente egreso, CuentaCorriente ingreso, double dinero) {
		ingreso.balance += dinero;
		egreso.balance -= dinero;
	}
	/*
	 * Otro modo 
	  public String imprimir() {
		return "\n Nombre de Titular = " + nomacc+ 
				"\n Balance = " + balance + 
				"\n Numero de Cuenta = " + numacc + "\n";
	}
	*/ 
	@Override
	public String toString() {
		return "CuentaCorriente [balance=" + balance + ", nomacc=" + nomacc + ", numacc=" + numacc + "]";
	}
}